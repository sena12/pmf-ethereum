# ETHEREUM RESOURCES REPO

**https://github.com/truffle-box/react-box**

**https://medium.com/coinmonks/step-by-step-approach-to-create-dapp-using-ethereum-reactjs-ipfs-part-1-42ea4cf69488**

**https://medium.com/@takleakshar/how-to-build-a-decentralized-full-stack-app-in-ethereum-and-react-42e63d45a208**

**https://cryptocup.io/** - great template


### Setup of environment:

1. Truffle framework
1. Metamask - addon for browser, ethereum wallet (12 secret words for you: *chaos dilemma switch rely sound wedding midnight ethics involve process defy green*)
1. Ganache - private local blockchain with GUI
1. Sublime text - dosta dobar editor jer ima ethereum package add-on

### About repo:

* root folder containes some tutorials and books about solidity and ethereum in general, and all code is in **app** folder
* app:
 1. **build/contracts** - .json that are created afrer build, they represent represent smart contract
 2. **client** - folder made by me, actual node.js app in react-redux framework, basically everything outside this folder is ethereum stuff (back-end), and inside here is front-end
 3. **contracts** - source code of smart contracts
 4. **migrations** - migration files for smart contract
 5. **test** - smart contract tests

### Starting of app:

* first you need to install and start Ganache
* than you need make sure that Ganache blockchain and web3.js listener are on the same port; you configure that in Ganache setting (upper left corner), and in the code in `./src/containers/App.jsx`in function `appInitialization`
* now navigate to home of web-app (remember home of web-app is `~\app\client` != home of app `~/app`), then run `npm install` to install all dependencies and than you can type `npm start` to actually start application
* configuration files are in home of web-app in `webpack.config.js` and `.babel`  


### Some random notes about truffle & solidity:

* truffle framework has a lot small snippets of code that are great for leaning, eg. `truffle unbox pet-shop`

* **migrations folder** simply contains migrations, code that defines properties for deloyment on ethereum network

* at the beginning of every smart contract you need to define what version of solidity will u use, `truffle version`command in terminal

* **truffle.js** file containes data about ethereum network itself

* `truffle deploy` command deploys written smart contracts on network (`truffle deploy --reset` if it shows that network is up to date, but you know you changed contract)
* `truffle console` - starts truffle console, it serves as live working environment for ethereum network; inside of it is javascript syntax; for the beginning you need to create instance of node (smart contract) you want to work with, you do that with command`contract_name.deployed().then(function(inst) { app = inst });` (notice first that this is **promise**)creates instance in variable `app`,

* basicaly same syntax goes for every contract/data you want to work with, eg. data `
app.data_name.then(function(inst) {variable_name_you_want_in_console = inst})`

* `app.address` returns all addresses

* `struct structure_name` - is same as interface in TS-u & C#-u

* mapping of structures is `mapping(_KeyType => _ValueType) mapName` - for more go to https://ethereum.stackexchange.com/questions/9893/how-does-mapping-in-solidity-work

* getting mapped stuff from truffle console goes like `app.mapName(__Key).toNumber()`

* **web3.js** je library s kojim komuniciras s sa blockchainom & smart contractomf, upisi u truffle console web3 i izbacit ce ti sve moguce funckije & mogucnosti **web3.eth** sve funckicije za ethereum blockchain *web3.eth.accounts* baca listu accounta **2X TAB klikni za moguce opcije opet kao i svugdje**

### Testing smart contract

* **IMPORTANT TO WRITE TESTS!!!**
* mocha & chai framework
* uses folder **tests** in framework for it, `*.js` file,
* `truffle test` command is for starting all the tests
* https://medium.com/@gus_tavo_guim/testing-your-smart-contracts-with-javascript-40d4edc2abed
