# UPUTE ZA OSNOVNO POSTAVLJENJE GETH-A
Geth (ili Go Ethereum) je jedna od tri osnovne implementacije Ethereum protokola pisana u C++-u i Pythonu. Instalacija se vrši pomoću PPA managera a za dodatne upute pogledaj [ovdje](https://ethereum.github.io/go-ethereum/install/).

Jos korisno: [Geth naredbe](https://github.com/ethereum/go-ethereum/wiki/Command-Line-Options)

## STVARANJE BLOCKCHAINA I MREZE
Za testiranje aplikacija treba nam Ethereum mreza i poseban blockchain. **Genesis block** je naziv za pocetni block u lancu, njega se rucno unosi (specifikacija je u .json fajlu). Primjer jednog filename.json genesis fajla je ovdje:
```
{
  "nonce": "0x0000000000000042",
  "timestamp": "0x0",
  "parentHash": "0x0000000000000000000000000000000000000000000000000000000000000000",
  "extraData": "0x0",
  "gasLimit": "0x8000000",
  "difficulty": "0x400",
  "mixhash": "0x0000000000000000000000000000000000000000000000000000000000000000",
  "coinbase": "0x3333333333333333333333333333333333333333",
  "alloc": { }
}
```
Detaljno znacenje specifikacija bloka je nebitno, jedino sto ga obiljezava je **parent hash** koji je jednak 0 i difficulty kod majnanja koji mozemo rucno postaviti na sto god zelimo. Sada zapravo trebamo inicijalizirati blockchain to cemo napravi sa geth naredbom:

` geth --datadir "/path/gdje/zelimo/blockchain" init "path/do/genesis.json" `

Flag *--datadir* specificira put gdje zelimo da nam se blockchain spremi. (**NAPOMENA!** naredba 'geth' samo ce poceti skidati stvarni ethereum blockchain s neta). Ova naredba stvara nekoliko datoteka u datoteci specificiranoj u datadir. U **~/geth/chaindata** nalazi se: **.ipc** datoteka koja je kao "rucka na koji se zeli spojiti cvor pri pokretanju", znaci fajl sadrzi sve bitno od zavrsnog stanja (ocito svaki cvor ima razlicito zavrsno stanje). Kod pokretanja opet networka se treba specificirati i flag *--ipcpath*. Sam blockchain se nalazi u **.ldb** fajlu. Pokretanje iterativne **geth Javascript console** u kojoj cemo zapravo raditi sve bitno sa lancem radimo sa naredbom:

` geth --datadir "/isti/kao/gore" --nodiscover --networkid "1234" console>>console.log `

Flag *--nodiscover* je bitan zato da se lanac nebi vidio na netu, uvijek ga ukljuci da nebi bio hakiran! *--networkid* mozemo staviti koji god broj zelimo, izrazito bitan jer se preko njega spajaju nodes na istu ethereum mrezu. **BITNO!** Jos se moze ukljuciti i *--port* kao flag pa se specificira port na kojem cvor slusa (defaultni je 30303), al je dobro ga promjenit jer **svaki node na mrezi mora imat svoj port**.
**Napomena** Sa ovom naredbom se samo pokrece cvor, na mrezi jos uvijek nema nikakvih usera na mrezi, a oni su potrebni da bi se moglo majnat (gradit lanac), te slati & primati ether. Useri se dodaju javascript naredbama u console.

## KORISTENJE GETH CONSOLE
Ima nekoliko api-a koji su nam bitni kod koristenja konzole (**admin.**, **personal.** - sve operacije oko usera, **eth.** - sve operacije oko blockchaina, **miner.**). Provjeravanje koje se sve naredbe nalaze je ista fora kao i na Linuxu naci npr. *admin.+TAB* da vidis sta sve mozes napisat. Neke bitne naredbe:

* `admin.nodeInfo` - informacije o cvoru kojeg upravo pokreces, Broj **enode** definira jedinstveni broj cvora na mrezi.
* `personal.newAccount()` - stvaranje accounta, return ove naredbe je broj accounta
* `eth.blockNumber` - vraca broj trenutnog bloka, genesis block je 0

**AKO OCES JOS JEDNU CONSOLE OTVORIT ZA ISTI CVOR!** je naredba ` get attach ipc:/path/do/ips/fajla `

## OTVARANJE JOS JEDNOG CVORA
Trebas opet inicijalizirati blockchain sa istim genesis fajlom, naredba ista samo stavi razliciti --datadir. Kao sto je receno treba **drukciji port** bit kod pokretanja ovog cvora, a **isti networkid**. Sada je pokrenut jos jedan cvor na istoj mrezi, ali ti cvorovi jos nisu povezani, to provjerimo sa naredbom ` admin.peers() ` gdje ces dobit prazan skup. To se dodaje tako da naredbom ` admin.addPeer("enode_number_kojeg_zelimo_povezat") `. Sada admin.peers vraca enode drugog cvora i mreza je zapravo povezana

## GRADNJA LANCA
* ` miner.start(1) ` - broj jedan oznacava kolko se koristi threadova kod majnanja, trebas imat account ako oces gradit lanac.
* ` miner.stop() ` - zaustavlja majanje
* ` eth.getBalance(eth.getBlock(eth.blockNumber).miner) ` - kao u javascriptu lako je ulancavati naredbe, npr. eth.blockNumber vraca broj trenutnog broja, eth.getBlock vraca informacije o bloku ciji mu broj damo, .miner vraca ID accounta koji je stvorio taj blok i onda eth.getBalance vraca stanje racuna ethera za dani ID accounta.
