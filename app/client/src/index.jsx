import React from 'react';
import ReactDOM from 'react-dom';
import "./index.scss";
import App from "./containers/app/app"
import configureStore from './utilities/rootReducer';
import { Provider } from 'react-redux'

const store = configureStore({});

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

