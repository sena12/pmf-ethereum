import React from 'react';
import './loadingScreen.scss';
import ethIcon from '../../../public/ethIcon.png';

export default class LoadingScreen extends React.PureComponent{
    render(){
        return(
            <div className={"loading"}>
                <div className={"icons"}>
                <img 
                    src={ethIcon}
                    className={"firstIcon"} 
                />
                <img 
                    src={ethIcon}
                    className={"secondIcon"} 
                />
                 <img 
                    src={ethIcon}
                    className={"thirdIcon"} 
                />
                </div>
                <div className={"sign"}>{this.props.label}</div>
            </div>
        );
    }
}