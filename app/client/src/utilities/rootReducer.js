import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { combineReducers } from 'redux';
import { appReducer } from '../containers/app/reducer';
import { composeWithDevTools } from "redux-devtools-extension";
import { createLogger } from "redux-logger";

export const rootReducer = 
combineReducers({
  app: appReducer
});

export default function configureStore(initialState) {
 return createStore(
  rootReducer,
  initialState,
  composeWithDevTools(applyMiddleware(..._getMiddleware()))
 );
}

function _getMiddleware() {
  const logger = createLogger({
    predicate: (getState, action) => {
      switch (action.type) {
        default: return true;
      }
    }
  });
  const middleware = [thunk, logger];
  return [...middleware];
}
