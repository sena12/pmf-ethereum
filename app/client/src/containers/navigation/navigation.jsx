import React from 'react';
import './navigation.scss';
import { connect } from 'react-redux';


export function mapStateToProps(store) {
    return{
        web3js: store.app.web3js 
    }
}

export function mapDispatchToProps(dispatch){
    return {

    }
}

class Navigation extends React.Component {
    render(){
        return(
            <div className={"navigation"}>
            <div> ETHEREUM PROJECT </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);