import * as Constants from "./constants";

export const appInitialization = (web3tostore) => (dispatch) => {
    dispatch({
        type: Constants.APP_INITIALIZATION_ACTION,
        payload: web3tostore
    })
}