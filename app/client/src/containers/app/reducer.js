import * as Constants from "./constants";

export function appReducer(
  state = {web3js: null, connectionSuccessful: false}, // initial state goes there
  action = { type: "", payload: null } // inital action goes there
) {
  switch (action.type) {
    case Constants.APP_INITIALIZATION_ACTION: 
      return { 
        ...state, 
       web3js: action.payload
      };
    default:
      return{ 
        ...state 
      };
    }
  }