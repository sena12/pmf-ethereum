import React from 'react';
import web3 from 'web3';
import promisify from 'util.promisify';
import LoadingScreen from "../../components/loadingScreen/loadingScreen";
import HomeScreen from "../homeScreen/homeScreen";
import Navigation from "../navigation/navigation";
import { connect } from 'react-redux';
import { appInitialization } from "./actions";
import "babel-polyfill"
import "./app.scss";

const mapStateToProps = (store) => {
  return{
      web3js: store.app.web3js
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    appInitialization: (web3store) => dispatch(appInitialization(web3store)),
  }
 };


class App extends React.Component {
  constructor (props) {
    super(props)
    this.state = { connectionSuccessful: false }
    const intervalCheck = setInterval(this.checkConnection.bind(this), 2000);
  }

  componentDidUpdate() {
    if(this.state.connectionSuccessful){
      clearInterval(this.intervalCheck)
    }
  }

appInitialization() {
    const web3Provider = new Web3.providers.HttpProvider('http://localhost:8545')
    const web3 = new Web3(web3Provider)
    this.props.appInitialization(web3)
  }


  // template for using web3js functions, you must promisify it & than await 
  // of course outside function must be async
  // you must bind function before using it => constructor
  // also possibly babel-polyfill must be included
  async checkConnection () {
    if(this.props.web3js != null && this.state.connectionSuccessful == false){
      const promise = promisify(this.props.web3js.eth.getAccounts)
      const accounts =  await promise().catch((ex) => { console.log(ex) })
      if(accounts) { this.setState({ connectionSuccessful: true }) }
    }
  }

  componentDidMount(){
    this.appInitialization();
  }

  customRender() {
    if(this.state.connectionSuccessful){
      return(<HomeScreen />)
    }
    else {
      return(<LoadingScreen label={"PLEASE WAIT, CONNECTING TO ETHEREUM NETWORK"}/>)
    }
  }

  render () {
    return (
        <div className="app">
          <Navigation />
          {this.customRender()}
        </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
