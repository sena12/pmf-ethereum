import { connect } from 'react-redux';
import React from 'react';
import Web3 from 'web3';
import './homeScreen.scss';

export function mapStateToProps(store) {
    return{
        web3js: store.app.web3js 
    }
}

export function mapDispatchToProps(dispatch){
    return {

    }
}

class HomeScreen extends React.Component {
    constructor (props) {
        super(props);
    }

    render() {
        return(
        <div className={"homeScreen"}>
            <p>APPLICATION LOADED SUCCESSFULLY<br />list of all accounts and balances on network:</p>
                {this.props.web3js.eth.accounts.map((account, index) => (
                    <ol key={index}>
                        {account} - {this.props.web3js.eth.getBalance(account).toString()}
                    </ol>))
                } 
        </div>
        ); 
    }
}
    export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);