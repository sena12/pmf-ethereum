# NOTES

### 1. BASICS

* always start with pragma and version of solidity you want to use (eg. `pragma solidity ^0.4.0;`, latest at the time you write is 0.4.24)

* `contract` is the key word which represents "class" in smart contract, file can have multiple contracts

* **difference** with js: declaration if variable is private/public/internal (if variable is internal it can be accessed in inherited contract) (access) comes after declaration of type so: `string private name;` and not like js `private string name;`

* signed/unsigned variable numbers `uint` for unsigned

* `function nameOfFunc () public returns (returnType){
 }`

### 2. INHERITANCE

* key word `is`, eg. `contract myFirstContract is Bank

`
